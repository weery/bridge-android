package me.coweery.bridge

import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.node.POJONode
import io.reactivex.Single
import me.coweery.bridge.models.Message
import me.coweery.codegen.MvpPresenter
import me.coweery.codegen.viewProxy.ViewProxy

class BridgeImpl(
    val serverUrl: String
) : BridgeCore(serverUrl), Bridge {

    companion object {
        const val CREATE_PRESENTER_METHOD_NAME = "new"
        val ATTACH_VIEW_METHOD_NAME = MvpPresenter::class.java.methods.first().name
    }

    override fun createPresenter(className: String): Single<String> {

        val id = msgId.getAndIncrement().toString()
        if (ws.send(
                objectMapper
                    .writeValueAsString(
                        Message(
                            id, null, CREATE_PRESENTER_METHOD_NAME, POJONode(
                                NewPresenter(className)
                            )
                        )
                    )
            )
        ) {
            return Single
                .create<String> {
                    callbacks[id] = WebSocketCallbackInfo(
                        String::class.java,
                        it
                    )
                }
        } else {
            return Single.error(IllegalStateException())
        }
    }

    override fun attachView(objectId: String, view: ViewProxy<*>) {

        ws.send(
            objectMapper.writeValueAsString(
                Message(null, objectId, ATTACH_VIEW_METHOD_NAME, POJONode(AttachViewArgs(view.id)))
            )
        )
        views[view.id] = view
    }

    override fun execute(objectId: String, methodName: String, args: JsonNode) {

        ws.send(
            objectMapper.writeValueAsString(
                Message(null, objectId, methodName, args)
            )
        )

        synchronized(this) {
            views
                .filterValues { it.view.get() == null }
                .keys
                .forEach {
                    views.remove(it)
                }
        }
    }

    private class NewPresenter(
        @JsonProperty("className")
        private val className: String
    )

    private class AttachViewArgs(
        @JsonProperty("viewId")
        private val viewId: String
    )
}