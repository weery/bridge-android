package me.coweery.bridge

import com.fasterxml.jackson.databind.JsonNode
import io.reactivex.Single
import me.coweery.codegen.viewProxy.ViewProxy

interface Bridge {

    fun createPresenter(className: String): Single<String>

    fun execute(objectId: String, methodName: String, args: JsonNode)

    fun attachView(objectId: String, view: ViewProxy<*>)

    companion object {

        fun getInstance(serverUrl: String): Bridge {
            return BridgeImpl(serverUrl)
        }
    }
}