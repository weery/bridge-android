package me.coweery.bridge

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import io.reactivex.SingleEmitter
import me.coweery.bridge.models.Message
import me.coweery.codegen.viewProxy.ViewProxy
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.WebSocket
import okhttp3.WebSocketListener
import java.util.concurrent.atomic.AtomicLong

open class BridgeCore(
    private val serverUrl: String
) {

    protected val client: OkHttpClient = OkHttpClient()
    protected val objectMapper = ObjectMapper()
    protected var msgId = AtomicLong(0)
    protected val callbacks = mutableMapOf<String, WebSocketCallbackInfo<*>>()
    protected val views = mutableMapOf<String, ViewProxy<*>>()

    protected val listener = object : WebSocketListener() {

        override fun onFailure(webSocket: WebSocket, t: Throwable, response: okhttp3.Response?) {
            t.printStackTrace()
        }

        override fun onMessage(webSocket: WebSocket?, text: String) {

            val message = objectMapper.readValue(text, Message::class.java)
            if (message.id != null) {
                message.body?.let {
                    callbacks[message.id]?.handle(it)
                }
                callbacks.remove(message.id)
            } else {
                val viewProxy = views[message.objectId] ?: return

                if (viewProxy.view.get() != null && message.body != null) {
                    viewProxy.execute(
                        message.method,
                        message.body
                    )
                }
            }
        }
    }
    protected val ws = client.newWebSocket(
        Request.Builder().url("ws://$serverUrl").build(),
        listener
    )


    protected inner class WebSocketCallbackInfo<T>(
        private val responseType: Class<T>,
        private val emitter: SingleEmitter<T>
    ) {

        fun handle(body: JsonNode) {

            try {
                emitter.onSuccess(objectMapper.treeToValue(body, responseType))
            } catch (e: Throwable) {
                emitter.onError(e)
            }
        }
    }
}