package me.coweery.bridge.models

import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.databind.JsonNode

class Message(

    @JsonProperty("id")
    val id: String? = null,

    @JsonProperty("objectId")
    val objectId: String? = null,

    @JsonProperty("method")
    val method: String,

    @JsonProperty("body")
    val body: JsonNode? = null
)